import 'package:flutter/material.dart';
import './register.dart';
import './resetpassword.dart';
import './dashboard.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;

  get _userNamefield => TextFormField(
        validator: (value) {
          if (value!.isEmpty) {
            return 'Please enter a valid e-mail';
          }
        },
        decoration: const InputDecoration(
            labelText: 'EMAIL',
            labelStyle: const TextStyle(
                fontWeight: FontWeight.bold, color: Colors.grey),
            focusedBorder: const UnderlineInputBorder(
                borderSide: const BorderSide(color: Colors.red))),
      );

  get _passwordField => TextFormField(
        validator: (value) {
          if (value!.isEmpty) {
            return 'Please enter the password';
          } else if (value.length <= 6) {
            return 'Please enter a valid password';
          }
        },
        decoration: const InputDecoration(
            labelText: 'PASSWORD',
            labelStyle: const TextStyle(
                fontWeight: FontWeight.bold, color: Colors.grey),
            focusedBorder: const UnderlineInputBorder(
                borderSide: const BorderSide(color: Colors.red))),
        obscureText: true,
      );

  get _forgotPassword => Container(
        alignment: const Alignment(1.0, 0.0),
        padding: const EdgeInsets.only(top: 15.0, left: 20.0),
        child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      ResetPasswordPage(title: 'Reset Password'),
                ));
          },
          child: const Text(
            'Forgot Password?',
            style: const TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue,
                decoration: TextDecoration.underline),
          ),
        ),
      );

  get _loginButton => Container(
        height: 40.0,
        child: Material(
          borderRadius: BorderRadius.circular(20.0),
          shadowColor: Colors.red,
          color: Colors.black,
          elevation: 1.0,
          child: RaisedButton(
              color: Colors.blue,
              onPressed: () {
                _isLoading = true;
                var isValid = _formKey.currentState!.validate();
                if (!isValid) {
                  _isLoading = false;
                }
                if (isValid) {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => DashboardPage()));
                  _isLoading = false;
                }
              },
              child: const Center(
                  child: const Text(
                'LOGIN',
                style:
                    TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              ))),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding:
                  const EdgeInsets.only(top: 35.0, left: 20.0, right: 20.0),
              child: Column(
                children: <Widget>[
                  _userNamefield,
                  const SizedBox(height: 20.0),
                  _passwordField,
                  const SizedBox(height: 20.0),
                  _forgotPassword,
                  const SizedBox(height: 40.0),
                  _loginButton,
                  //_loadingView
                ],
              ),
            ),
            const SizedBox(height: 15.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Text(
                  'MTN APP ?',
                  style: const TextStyle(),
                ),
                const SizedBox(
                  width: 5.0,
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (BuildContext context) =>
                              new RegisterPage(title: 'Register'),
                        ));
                    //Navigator.of(context).pushNamed('/register');
                  },
                  child: const Text('Register',
                      style: const TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline)),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
