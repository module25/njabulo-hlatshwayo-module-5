import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final _backgroundColor = Colors.white;
  final _profileUrl = 'MTN';
  // 'MTN APP';

  bool isFavourite = false;
  _togglFavourites() {
    setState(() {
      isFavourite = !isFavourite;
    });
  }

  get _lauchWebsite => OutlineButton(
        color: Colors.red,
        onPressed: () {
          _launchURL();
        },
        child: Text('View',
            style: TextStyle(
              color: Colors.grey,
              fontSize: 14.0,
            )),
      );

  get _userName => Text(
        'Njabulo Hlatshwayo',
        style: TextStyle(
            color: Colors.grey, fontSize: 24.0, fontWeight: FontWeight.bold),
      );

  get _userRole => Text(
        'Software Engineer',
        style: TextStyle(
            color: Colors.grey[400],
            fontSize: 16.0,
            fontWeight: FontWeight.w500),
      );

  get _userDescription => Text(
        'Flutter Developer',
        style: TextStyle(
            color: Colors.grey[400],
            fontSize: 20.0,
            fontWeight: FontWeight.w500),
      );

  get _userAvatar => Hero(
        tag: 'Njabulo',
        child: CircleAvatar(
          maxRadius: 80.0,
          backgroundImage: NetworkImage(_profileUrl),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: _backgroundColor,
      appBar: AppBar(
        title: Text('Profile edit'),
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(isFavourite ? Icons.favorite : Icons.favorite_border),
            onPressed: () {
              _togglFavourites();
            },
          ),
          IconButton(
            icon: Icon(Icons.more_vert),
            onPressed: () {},
          )
        ],
      ),
      body: Container(
        width: double.maxFinite,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            _userAvatar,
            SizedBox(height: 10.0),
            _userName,
            SizedBox(height: 5.0),
            _userRole,
            _userDescription,
            SizedBox(height: 5.0),
            _lauchWebsite,
          ],
        ),
      ),
    );
  }
}

_launchURL() async {
  const url = 'MTN APP';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

launch(String url) {}

canLaunch(String url) {}
