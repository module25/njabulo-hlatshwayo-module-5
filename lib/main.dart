import 'package:flutter/material.dart';
import './ui/login.dart';
import './ui/register.dart';
import 'login.dart';
import 'register.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: "AIzaSyA3CYRi2A9g0tN2AgL9WOSfshFr7sVvdrw",
          authDomain: "module-5-ee463.firebaseapp.com",
          projectId: "module-5-ee463",
          storageBucket: "module-5-ee463.appspot.com",
          messagingSenderId: "439527071135",
          appId: "1:439527071135:web:3d2cf9daed2d0a6eef71aa"));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MTN APP',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        'register': (context) => RegisterPage(
              title: '',
            )
      },
      home: LoginPage(title: 'Login'),
      debugShowCheckedModeBanner: false,
    );
  }
}
